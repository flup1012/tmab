# TMAB

## What ?

TMAB stands for True Music Addon Builder. It is a tool to help automate the creation
of an addon for the [True Music Mod](https://steamcommunity.com/sharedfiles/filedetails/?id=2613146550).

It supports adding music as "Cassettes" or "Vinyl". It also supports adding Vinyl cover, with images transcoding
and resizing.

This tool is open source and works on all major platforms as it is written in pure Go.

## How ?

### Initialize

First initialize a new mod:

```shell
/path/to/tmab build ~/path/to/yourMod
```

If the directory does not exist it will ask if it should be created.
If the directory exists it will test if the `mod.info` file exists and
propose to init the directory as a new mod.

### Build

Once you have a mod directory, you can add ogg files inside the "TCBoombox" and "TCVinylplayer"
BEWARE: if you want True Music to be able to read your music correctly you MUST remove embedded
images from them, or you'll just get a small garbled sound when trying to play your songs.

tmab tries to detect images inside ogg containers and warn you.

You can add covers to your vinyls by dropping image files with the same name as the ogg file in the
"TCVinylplayer" directory. Supported extensions are: "gif", "jpeg", "jpg", "png". Make sure the image is a 1:1 square
format for better results, as the program will resize the images to 300x300 to create game meshes.

Once you are happy with your playlist, just run tmab on your mod dir again:

```shell
/path/to/tmab build ~/path/to/yourMod
```

The `yourMod` folder is the folder that contains a `mod.info` file, a `poster.png` and a `media` dir.

Once this is done you'll get new files in your `media/lua`, `media/scripts`, `media/textures` folders. Those folders
are fully handled by tmab. You cannot change anything manually as they will be destroyed and re-created from scratch
each time.

You will note that the files have an uuid in their name. This is an automatic process that allows your True Music Addon
to be compatible with any other True Music Addon without any risk of filename collision.

### Note for Windows users

You do not need to open a command line to run tmab. Just create an empty directory anywhere you like and just
drag and drop the empty dir on the tmab exe. It will ask you if you want to initialize it. Just write `y` or
any combination of `yes`, `Yes`, `YES` then press `ENTER`.
This will initialize your mod folder. Follow the instructions in the build section above about adding ogg files
and some optional covers for your vinyls. Then drag and drop the same folder on the tmab exe again. And Voilà!

## Why ?

The True Music Addon authors provided a nice program that did nearly the same job, but it unfortunately only runs on
Windows with Dotnet 4 installed. Since I am using Linux all the time I wanted to be able to use my work and gaming rig
to pick my addon playlists and build, test them.
Since I could not install Dotnet 4 under Wine I decided to rewrite a multi-platform program to help me and others.

In the process I added more quality of life:

  - image auto-resizing in multiple input formats (original program only supports jpg)
  - generated files auto mangle (auto add a different uuid for the generated files). This was something you had to do
    manually and rapidly became quite annoying.
  - scan ogg files for embedded images to warn user of "invalid" media files
  - generating the mod.info file from the root of your mod.

## Roadmap

I'd like to add support for:

  - auto "clean" the input file names to remove characters True Music does not understand
  - generating the full workshop item architecture instead of just the local mod
    this would open the way to get the mod id and avoid UUID gen the file names.
