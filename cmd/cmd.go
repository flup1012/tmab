package cmd

import (
	"fmt"
	"os"

	"github.com/jessevdk/go-flags"
	tmab "gitlab.com/flup1012/tmab/lib"
	"gitlab.com/flup1012/tmab/lib/logging"
)

var (
	Version   = tmab.Version()
	CommitSha = tmab.CommitSha()
	BuildDate = tmab.BuildDate()
)

var (
	Logger         = logging.DefaultLogger(os.Stdout)
	LoggingOptions = logging.MustOptions(logging.NewOptions(&Logger, os.Stdout))

	parser = flags.NewNamedParser("tmab", flags.HelpFlag|flags.PassDoubleDash)
)

func Run() int {
	Logger = getLogger(LoggingOptions)
	var cmdFound bool
	var isHelp bool
	if len(os.Args) == 2 {
		for _, cmd := range parser.Commands() {
			if cmd.Name == os.Args[1] {
				cmdFound = true
				// a normal command is called
				break
			}
		}
		if os.Args[1] == "-h" {
			isHelp = true
		}
		if !cmdFound && !isHelp {
			// here we are in the default command with a positional arg
			bCmd := parser.Find("build")
			if bCmd == nil {
				panic("failed to find the build cmd")
			}
			buildCmd := NewBuildCmd(LoggingOptions)
			buildCmd.PositionalArgs.RunDir = os.Args[1]
			buildCmd.Args.MusicDir = defaultMusicDir
			if err := buildCmd.Execute([]string{"autoCmd"}); err != nil {
				os.Exit(1)
			}
			os.Exit(0)
		}
	}
	if _, err := parser.Parse(); err != nil {
		code := 1
		if fe, ok := err.(*flags.Error); ok { // nolint:errorlint
			if fe.Type == flags.ErrHelp {
				code = 0
				// this error actually contains a help message for the user
				// so we print it on the console
				fmt.Println(err)
			} else {
				Logger.Error().Msg(err.Error())
			}
		} else {
			Logger.Err(err).Msg("")
		}
		return code
	}
	return 0
}
