//go:build !windows

package cmd

import (
	"os"

	"github.com/rs/zerolog"
	"gitlab.com/flup1012/tmab/lib/logging"
)

func getLogger(opts *logging.Options) zerolog.Logger {
	l := opts.Logger()
	return l.Output(zerolog.ConsoleWriter{
		Out:     os.Stdout,
		NoColor: false,
	})
}
