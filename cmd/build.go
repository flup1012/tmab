package cmd

import (
	"github.com/rs/zerolog"
	"gitlab.com/flup1012/tmab/lib/logging"
	"gitlab.com/flup1012/tmab/runner"
)

const defaultMusicDir = "yourMusic"

type BuildCmd struct {
	Args struct {
		MusicDir string `short:"m" long:"musicdir" description:"name of the folder that contains your TCBoombox a TCVinylplayer dirs" default:"yourMusic"`
	}
	PositionalArgs struct {
		RunDir string `required:"yes" positional-arg-name:"rundir"`
	} `positional-args:"yes"`
	log zerolog.Logger `no-flag:"t"`
}

// NewBuildCmd ...
func NewBuildCmd(loggingOptions *logging.Options) *BuildCmd {
	cmd := BuildCmd{log: getLogger(loggingOptions)}
	return &cmd
}

// Execute ...
func (cmd *BuildCmd) Execute(args []string) error {
	var autoCmd = false
	for _, a := range args {
		if a == "autoCmd" {
			autoCmd = true
		}
	}
	musicDir := cmd.Args.MusicDir
	runDir := cmd.PositionalArgs.RunDir
	cmd.log.Debug().
		Str("run dir", runDir).
		Str("music dir", musicDir).
		Msg("starting tmab build")

	config := runner.NewConfig(cmd.log, runDir, musicDir)

	r := runner.NewRunner(config)
	return r.Build(autoCmd)
}

func init() {
	buildCmd := NewBuildCmd(LoggingOptions)
	_, err := parser.AddCommand("build", "Build a true music addon", "", buildCmd)
	if err != nil {
		Logger.Fatal().Msg(err.Error())
	}

	g, err := parser.AddGroup("Logging", "Logging options", LoggingOptions)
	if err != nil {
		panic(err)
	}
	g.Namespace = "log"
}
