package cmd

import (
	"github.com/rs/zerolog"
	"gitlab.com/flup1012/tmab/lib/logging"
	"gitlab.com/flup1012/tmab/runner"
)

type InitCmd struct {
	Args struct {
		MusicDir string `short:"m" long:"musicdir" description:"name of the folder that contains your TCBoombox a TCVinylplayer dirs" default:"yourMusic"`
	}
	PositionalArgs struct {
		RunDir string `required:"yes" positional-arg-name:"rundir"`
	} `positional-args:"yes"`
	log zerolog.Logger `no-flag:"t"`
}

// NewInitCmd ...
func NewInitCmd(loggingOptions *logging.Options) *InitCmd {
	cmd := InitCmd{log: getLogger(loggingOptions)}
	return &cmd
}

// Execute ...
func (cmd *InitCmd) Execute([]string) error {
	musicDir := cmd.Args.MusicDir
	runDir := cmd.PositionalArgs.RunDir
	cmd.log.Debug().
		Str("run dir", runDir).
		Str("music dir", musicDir).
		Msg("starting tmab Init")

	config := runner.NewConfig(cmd.log, runDir, musicDir)

	r := runner.NewRunner(config)
	return r.InitializeWorkshopDir()
}

func init() {
	initCmd := NewInitCmd(LoggingOptions)
	_, err := parser.AddCommand("init", "Init a true music addon from scratch", "", initCmd)
	if err != nil {
		Logger.Fatal().Msg(err.Error())
	}
}
