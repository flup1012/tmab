# github.com/dhowden/tag v0.0.0-20220618230019-adf36e896086
## explicit; go 1.18
github.com/dhowden/tag
# github.com/getsentry/sentry-go v0.20.0
## explicit; go 1.18
github.com/getsentry/sentry-go
github.com/getsentry/sentry-go/internal/debug
github.com/getsentry/sentry-go/internal/otel/baggage
github.com/getsentry/sentry-go/internal/otel/baggage/internal/baggage
github.com/getsentry/sentry-go/internal/ratelimit
# github.com/jessevdk/go-flags v1.5.0
## explicit; go 1.15
github.com/jessevdk/go-flags
# github.com/m4rw3r/uuid v1.0.1
## explicit
github.com/m4rw3r/uuid
# github.com/mattn/go-colorable v0.1.13
## explicit; go 1.15
github.com/mattn/go-colorable
# github.com/mattn/go-isatty v0.0.17
## explicit; go 1.15
github.com/mattn/go-isatty
# github.com/rs/zerolog v1.29.0
## explicit; go 1.15
github.com/rs/zerolog
github.com/rs/zerolog/internal/cbor
github.com/rs/zerolog/internal/json
github.com/rs/zerolog/log
# golang.org/x/image v0.7.0
## explicit; go 1.12
golang.org/x/image/draw
golang.org/x/image/math/f64
# golang.org/x/sys v0.7.0
## explicit; go 1.17
golang.org/x/sys/execabs
golang.org/x/sys/internal/unsafeheader
golang.org/x/sys/plan9
golang.org/x/sys/unix
golang.org/x/sys/windows
# golang.org/x/term v0.7.0
## explicit; go 1.17
golang.org/x/term
# golang.org/x/text v0.9.0
## explicit; go 1.17
golang.org/x/text/cases
golang.org/x/text/internal
golang.org/x/text/internal/language
golang.org/x/text/internal/language/compact
golang.org/x/text/internal/tag
golang.org/x/text/language
golang.org/x/text/transform
golang.org/x/text/unicode/norm
