package main

import (
	"os"

	"gitlab.com/flup1012/tmab/cmd"
)

func main() {
	if code := cmd.Run(); code != 0 {
		os.Exit(code)
	}
}
