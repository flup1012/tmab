package runner

import (
	"errors"
	"fmt"
	"strings"
)

func (r Runner) askForConfirmation() (bool, error) {
	var response string

	_, err := fmt.Scanln(&response)
	if err != nil {
		r.config.Log.Err(err).Msg("failed to scan user answer")
		return false, errors.New("could not parse user answer")
	}

	switch strings.ToLower(response) {
	case "y", "yes":
		return true, nil
	case "n", "no":
		return false, nil
	default:
		r.config.Log.Warn().Msg("I'm sorry but I didn't get what you meant, please type (y)es or (n)o and then press enter:")
		return r.askForConfirmation()
	}
}

// AskForKeyEnterPress is used to block until user presses ENTER key
// this is typically used on Windows platform to allow for users
// using drag & drop on the exe to be able to read the output
func (r Runner) AskForKeyEnterPress() error {
	var response string
	fmt.Println("press ENTER to continue")
	_, err := fmt.Scanln(&response)
	if err != nil {
		r.config.Log.Err(err).Msg("failed to get user answer")
		return errors.New("could not get user answer")
	}
	return nil
}
