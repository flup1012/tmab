//go:build windows

package runner

func (r Runner) Finalize(autoCmd bool) error {
	// on Windows want to block
	// to allow users to see the cmd box before it closes
	if autoCmd {
		// but only block if auto cmd is used, not if the program
		// was called by someone using real command line args
		return r.AskForKeyEnterPress()
	}
	return nil
}
