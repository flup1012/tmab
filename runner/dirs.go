package runner

import (
	"errors"
	"fmt"
	"os"
	"path/filepath"

	"github.com/rs/zerolog"
)

// exists returns whether the given file or directory exists
func exists(path string) (bool, error) {
	_, err := os.Stat(path)
	if err == nil {
		return true, nil
	}
	if errors.Is(err, os.ErrNotExist) {
		return false, nil
	}
	return false, err
}

func dirExists(path string) (bool, error) {
	st, err := os.Stat(path)
	if errors.Is(err, os.ErrNotExist) {
		return false, nil
	}
	if err == nil {
		if st.IsDir() {
			return true, nil
		}
	}
	return false, err
}

func removeAll(log zerolog.Logger, path string) error {
	dirExists, err := exists(path)
	if err != nil {
		log.Err(err).Msg(fmt.Sprintf("failed to stat dir: %s", path))
		return err
	}

	if dirExists {
		if err := os.RemoveAll(path); err != nil {
			log.Err(err).Msg(fmt.Sprintf("failed to remove old scripts dir: %s", path))
		}
	}
	return nil
}

func (r Runner) ensureEmptyTextureDir(dirMod os.FileMode) error {
	// remove old textures
	mediaDir := r.ActualMediaDir()
	texturesDir := filepath.Join(mediaDir, "textures")
	if err := removeAll(r.config.Log, texturesDir); err != nil {
		return err
	}
	if err := os.Mkdir(texturesDir, dirMod); err != nil {
		r.config.Log.Err(err).Str("dir", texturesDir).Msg("failed to create dir")
		return err
	}
	worldItemsDir := filepath.Join(texturesDir, "WorldItems")
	if err := os.Mkdir(worldItemsDir, dirMod); err != nil {
		r.config.Log.Err(err).Str("dir", worldItemsDir).Msg("failed to create dir")
		return err
	}
	return nil
}

func (r Runner) ensureEmptyScriptsDir(dirMod os.FileMode) error {
	mediaDir := r.ActualMediaDir()
	// remove old scripts
	scriptsDir := filepath.Join(mediaDir, "scripts")
	if err := removeAll(r.config.Log, scriptsDir); err != nil {
		return err
	}
	if err := os.Mkdir(scriptsDir, dirMod); err != nil {
		r.config.Log.Err(err).Str("dir", scriptsDir).Msg("failed to create dir")
		return err
	}
	return nil
}

func (r Runner) ensureEmptyLuaDir(dirMod os.FileMode) error {
	mediaDir := r.ActualMediaDir()
	// remove old lua dir
	luaDir := filepath.Join(mediaDir, "lua")
	if err := removeAll(r.config.Log, luaDir); err != nil {
		return err
	}
	if err := os.Mkdir(luaDir, dirMod); err != nil {
		r.config.Log.Err(err).Str("dir", luaDir).Msg("failed to create dir")
		return err
	}
	serverDir := filepath.Join(luaDir, "server")
	if err := os.Mkdir(serverDir, dirMod); err != nil {
		r.config.Log.Err(err).Str("dir", serverDir).Msg("failed to create dir")
		return err
	}
	itemsDir := filepath.Join(serverDir, "Items")
	if err := os.Mkdir(itemsDir, dirMod); err != nil {
		r.config.Log.Err(err).Str("dir", serverDir).Msg("failed to create dir")
		return err
	}
	sharedDir := filepath.Join(luaDir, "shared")
	if err := os.Mkdir(sharedDir, dirMod); err != nil {
		r.config.Log.Err(err).Str("dir", sharedDir).Msg("failed to create dir")
		return err
	}
	return nil
}

func (r Runner) cleanMediaAutoDirs() error {
	var dirMod os.FileMode = 0744

	if err := r.ensureEmptyTextureDir(dirMod); err != nil {
		return err
	}

	if err := r.ensureEmptyScriptsDir(dirMod); err != nil {
		return err
	}

	if err := r.ensureEmptyLuaDir(dirMod); err != nil {
		return err
	}

	return nil
}

func (r Runner) createMusicDirIfNotExist(dirMod os.FileMode) error {
	musicDir := r.ActualMusicDir()
	dirExists, err := exists(musicDir)
	if err != nil {
		return err
	}
	if !dirExists {
		if err := os.Mkdir(musicDir, dirMod); err != nil {
			return err
		}
		tcBoomDir := filepath.Join(musicDir, "TCBoombox")
		if err := r.createDirIfNotExists(tcBoomDir, dirMod); err != nil {
			return err
		}
		tcVinylDir := filepath.Join(musicDir, "TCVinylplayer")
		if err := r.createDirIfNotExists(tcVinylDir, dirMod); err != nil {
			return err
		}
	}
	return nil
}

func (r Runner) createDirIfNotExists(dir string, dirMod os.FileMode) error {
	dirExists, err := exists(dir)
	if err != nil {
		return err
	}
	if dirExists {
		return nil
	}
	if err := os.Mkdir(dir, dirMod); err != nil {
		r.config.Log.Err(err).Str("dir", dir).Msg("failed to create dir")
		return err
	}
	return nil
}

func (r Runner) ModDir() string {
	return filepath.Join(r.WorkshopRootDir(), "Contents", "mods", "truemusic_addon")
}

func (r Runner) WorkshopRootDir() string {
	return r.config.RunDir
}
