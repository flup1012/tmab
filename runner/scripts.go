package runner

import (
	"crypto/rand"
	"fmt"
	"math/big"
	"path/filepath"
	"strings"
	"text/template"

	"github.com/rs/zerolog"
)

// GameMedia is the struct that holds a representation of the lua object
// either Cassette or Vinyl
type GameMedia struct {
	Name             string
	Type             string
	Weight           string
	Icon             string
	DisplayName      string
	WorldStaticModel string
}

type MediaType int

const (
	MediaTypeTape MediaType = iota
	MediaTypeVinyl
)

// genIcon creates a string in the form of TCTape1 where 1 is a
// number between 1 and 12 and sets it as its icon
func (c *GameMedia) genIcon(mediaType MediaType) {
	var max int64 = 12
	var min int64 = 1
	number, err := rand.Int(rand.Reader, big.NewInt(max))
	if err != nil {
		panic("failed to read from random source")
	}
	if mediaType == MediaTypeTape {
		c.Icon = fmt.Sprintf("TCTape%d", number.Int64()+min)
	} else {
		c.Icon = fmt.Sprintf("TCVinylrecord%d", number.Int64()+min)
	}
}

// genWorkStaticModel create a static model string corresponding
// to the given icon
func (c *GameMedia) genWorldStaticModel(cover *Cover) {
	if cover == nil {
		c.WorldStaticModel = fmt.Sprintf("Tsarcraft.%s", c.Icon)
	} else {
		c.WorldStaticModel = fmt.Sprintf("Tsarcraft.%s", cover.ItemName)
	}
}

func (c *GameMedia) setWeight() {
	// TODO: use a config var ?
	c.Weight = "0.02"
}

var zeroOutStrings = []string{
	" ",
	".",
	",",
	"-",
	"?",
	"!",
	"'",
	"\"",
}

func getMediaTypeName(mediaType MediaType) string {
	var name string
	switch mediaType {
	case MediaTypeTape:
		name += "Cassette"
	case MediaTypeVinyl:
		name += "Vinyl"
	default:
		panic("unsupported media type")
	}
	return name
}

func genUniqueName(songName string, mediaType MediaType, suffix string) string {
	name := getMediaTypeName(mediaType)

	for idx, zs := range zeroOutStrings {
		switch {
		case idx == 0:
			name += strings.ReplaceAll(songName, zs, "")
		default:
			name = strings.ReplaceAll(name, zs, "")
		}
	}
	name += suffix
	return name
}

func NewGameMedia(uniqueName, songName string, mediaType MediaType, cover *Cover) *GameMedia {
	displayName := getMediaTypeName(mediaType)
	displayName += " " + songName

	c := &GameMedia{DisplayName: displayName, Type: "Normal", Name: uniqueName}
	c.genIcon(mediaType)
	c.genWorldStaticModel(cover)
	c.setWeight()
	return c
}

// PlayerScriptTemplate will be used for VinylPlayer and BoomBox
var PlayerScriptTemplate = `module Tsarcraft
{
	imports
	{
		Base
	}
/*************** Generated Music Carriers ****************/
  {{range .Covers -}}
  model {{.ItemName}}
  {
	mesh		=	WorldItems/TCCover,
	texture		=	WorldItems/{{.Name}},
	scale		=	0.0012,
  }
  {{end}}
  {{range .Medias -}}
  item {{.Name}}
  {
	Type				= {{.Type}},
	DisplayCategory			= Entertainment,
	Weight				= {{.Weight}},
	Icon				= {{.Icon}},
	DisplayName			= {{.DisplayName}},
	WorldStaticModel		= {{.WorldStaticModel}},
  }
  {{end}}
}
`

func getPlayerScriptTemplate(log zerolog.Logger) (*template.Template, error) {
	return getTemplate(log, "scriptplayer", PlayerScriptTemplate)
}

type Sound struct {
	Name     string
	FileName string
	MusicDir string
}

func withoutExtension(fileName string) string {
	ext := filepath.Ext(fileName)
	return strings.TrimSuffix(fileName, ext)
}

func NewSound(uniqueName, fileName, musicDir string) *Sound {
	return &Sound{
		Name:     uniqueName,
		FileName: fileName,
		MusicDir: musicDir,
	}
}

var SoundTemplate = `module Tsarcraft
{
	{{range . -}}
	sound {{.Name}}
	{
		category = True Music,
		master = Ambient,
		clip
		{
			file = media/{{.MusicDir}}/{{.FileName}},
			distanceMax = 75,
		}
	}
	{{ end}}
}
`

func getSoundTemplate(log zerolog.Logger) (*template.Template, error) {
	return getTemplate(log, "scriptsound", SoundTemplate)
}
