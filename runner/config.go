package runner

import "github.com/rs/zerolog"

type Config struct {
	Log      zerolog.Logger
	RunDir   string
	MusicDir string
}

func NewConfig(log zerolog.Logger, runDir string, musicDir string) *Config {
	return &Config{
		Log:      log,
		RunDir:   runDir,
		MusicDir: musicDir,
	}
}
