package runner

import (
	"io"

	"github.com/dhowden/tag"
	"github.com/rs/zerolog"
)

// Metadata represents a media metadata content
type Metadata struct {
	Log      zerolog.Logger
	metaData tag.Metadata
}

// NewMetadata is a constructor
func NewMetadata(source io.ReadSeeker) (*Metadata, error) {
	m := Metadata{}
	metaData, err := tag.ReadFrom(source)
	if err != nil {
		m.Log.Err(err).Msg("failed to read from source")
		return nil, err
	}
	m.metaData = metaData
	return &m, nil
}

// HasPicture returns true if the metadata contains a picture
// and false otherwise
func (m Metadata) HasPicture() bool {
	return m.metaData.Picture() != nil
}
