package runner

import (
	"image"
	// add support for gif format
	_ "image/gif"
	// add support for jpeg
	_ "image/jpeg"
	"image/png"
	"io"
	"os"
	"path/filepath"

	"github.com/rs/zerolog"
	"golang.org/x/image/draw"
)

// Cover is a media that represents a Vinyl cover
type Cover struct {
	Name     string // used for file name for the media
	ItemName string // used for item name in lua scripts
	log      zerolog.Logger
	img      image.Image // real image data
}

// Covers are a list of Cover
type Covers []*Cover

func (c *Cover) fromFile(fileName string) error {
	reader, err := os.Open(fileName)
	if err != nil {
		return err
	}
	defer func() {
		if err := reader.Close(); err != nil {
			c.log.Err(err).Msg("failed to close image reader")
		}
	}()
	img, _, err := image.Decode(reader)
	if err != nil {
		c.log.Err(err).Str("file", fileName).Msg("failed to decode cover image")
		return err
	}
	dr := image.Rect(0, 0, 300, 300)
	scaled := scaleTo(img, dr, draw.BiLinear)
	c.img = scaled
	return nil
}

func (c *Cover) ToMediaFile(outFile io.Writer) error {
	e := png.Encoder{
		CompressionLevel: -3,
	}
	if err := e.Encode(outFile, c.img); err != nil {
		c.log.Err(err).Str("cover", c.Name).Msg("failed to encode cover png")
		return err
	}
	return nil
}

func NewCover(log zerolog.Logger, fileName string) (*Cover, error) {
	c := &Cover{log: log}
	c.Name = filepath.Base(withoutExtension(fileName))
	// media type is always Vinyl for a cover
	c.ItemName = genUniqueName(c.Name, MediaTypeVinyl, "Model")

	if err := c.fromFile(fileName); err != nil {
		log.Err(err).Msg("failed to load cover image")
		return nil, err
	}
	return c, nil
}

func scaleTo(src image.Image,
	rect image.Rectangle, scale draw.Scaler) image.Image {
	dst := image.NewRGBA(rect)
	scale.Scale(dst, rect, src, src.Bounds(), draw.Over, nil)
	return dst
}
