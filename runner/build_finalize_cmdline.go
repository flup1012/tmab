//go:build !windows

package runner

func (r Runner) Finalize(autoCmd bool) error {
	// nothing special on non-windows platforms...
	return nil
}
