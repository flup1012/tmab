package runner

import (
	"os"
	"text/template"

	"github.com/rs/zerolog"
)

// ModInfo represents the mod.info file found at the root
// of you mod dir
type ModInfo struct {
	Log zerolog.Logger

	Name        string
	ID          string
	Poster      string
	Require     string
	Description string
}

var modInfoTemplate = `name={{.Name}}
poster={{.Poster}}
id={{.ID}}
require={{.Require}}
description={{.Description}}`

func (m ModInfo) template() (*template.Template, error) {
	return getTemplate(m.Log, "modInfoTemplate", modInfoTemplate)
}

func (m ModInfo) Serialize(fName string) error {
	f, err := os.OpenFile(fName, os.O_TRUNC|os.O_CREATE|os.O_RDWR, 0644)
	if err != nil {
		return err
	}
	defer func() {
		if err := f.Close(); err != nil {
			m.Log.Err(err).Str("file", fName).Msg("failed to close file")
		}
	}()

	tmpl, err := m.template()
	if err != nil {
		return err
	}
	if err := tmpl.Execute(f, m); err != nil {
		return err
	}
	return nil
}

func NewModInfo(log zerolog.Logger, name, id, poster, require, description string) ModInfo {
	return ModInfo{
		Log:         log,
		Name:        name,
		ID:          id,
		Poster:      poster,
		Require:     require,
		Description: description,
	}
}
