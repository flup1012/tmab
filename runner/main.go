package runner

import (
	"errors"
	"fmt"
	"image"
	"image/color"
	"image/png"
	"os"
	"path/filepath"
	"strings"

	"github.com/m4rw3r/uuid"
)

const (
	VinylplayerDir = "TCVinylplayer"
	BoomboxDir     = "TCBoombox"
)

type Runner struct {
	config *Config
}

func NewRunner(c *Config) *Runner {
	return &Runner{config: c}
}

func (r Runner) WorkshopName() string {
	return filepath.Base(r.WorkshopRootDir())
}

func (r Runner) ActualMediaDir() string {
	return filepath.Join(r.ModDir(), "media")
}

func (r Runner) ActualMusicDir() string {
	return filepath.Join(r.ActualMediaDir(), r.config.MusicDir)
}

func (r Runner) ActualBoomBoxDir() string {
	return filepath.Join(r.ActualMusicDir(), BoomboxDir)
}

func (r Runner) ActualVinylPlayerDir() string {
	return filepath.Join(r.ActualMusicDir(), VinylplayerDir)
}

func (r Runner) workshopDirExists() (bool, error) {
	exists, err := dirExists(r.WorkshopRootDir())
	if err != nil {
		return false, err
	}
	return exists, nil
}

func (r Runner) ProposeCreateIfNotExistingWorkshopDir() error {
	exists, err := r.workshopDirExists()
	if err != nil {
		return err
	}
	if exists {
		return nil
	}
	// here we should ask user if she wants to create named dir
	fmt.Printf("specified dir (%s) does not exist, do you want to create ?\n", r.WorkshopRootDir())
	resp, err := r.askForConfirmation()
	if err != nil {
		return err
	}
	if resp {
		// user said yes to create dir
		if err := r.createWorkshopDir(); err != nil {
			return err
		}
		// also init here
		// since the dir is fully empty
		return r.InitializeWorkshopDir()
	}
	err = errors.New("specified dir does not exist. Cannot continue")
	r.config.Log.Err(err).Str("dir", r.WorkshopRootDir()).Msg("dir does not exist")
	return err
}

func (r Runner) createWorkshopDir() error {
	var dirMod os.FileMode = 0744
	return os.Mkdir(r.WorkshopRootDir(), dirMod)
}

func (r Runner) initNeeded() (bool, error) {
	return r.workshopTXTExists()
}

func (r Runner) proposeInitIfNeeded() error {
	initNeeded, err := r.initNeeded()
	if err != nil {
		return err
	}
	if !initNeeded {
		fmt.Println("do you want to initialize dir: " + r.WorkshopRootDir() + " ?")
		resp, err := r.askForConfirmation()
		if err != nil {
			return err
		}
		if !resp {
			err := errors.New("workshop dir must be initialized. aborting")
			r.config.Log.Err(err).Str("dir", r.config.RunDir).Msg("workshop dir must be initialized")
			return err
		}
		return r.InitializeWorkshopDir()
	}
	return nil
}

func (r Runner) Build(autoCmd bool) error {
	if err := r.ProposeCreateIfNotExistingWorkshopDir(); err != nil {
		return err
	}

	if err := r.proposeInitIfNeeded(); err != nil {
		return err
	}

	if err := r.cleanMediaAutoDirs(); err != nil {
		return err
	}

	// process tapes
	if err := r.ProcessDir(MediaTypeTape); err != nil {
		return err
	}
	// process vinyls
	if err := r.ProcessDir(MediaTypeVinyl); err != nil {
		return err
	}

	return r.Finalize(autoCmd)
}

func fileExists(fName string) (bool, error) {
	fi, err := os.Stat(fName)
	if err != nil {
		if errors.Is(err, os.ErrNotExist) {
			return false, nil
		}
		return false, err
	}
	if fi.IsDir() {
		return false, errors.New(fName + "should be a file but is a directory")
	}
	return true, nil
}

func (r Runner) workshopTXTExists() (bool, error) {
	fName := filepath.Join(r.WorkshopRootDir(), "workshop.txt")
	return fileExists(fName)
}

// ModInfoExists tests is the mod.info file is present in the rundir
func (r Runner) ModInfoExists() (bool, error) {
	fName := filepath.Join(r.ModDir(), "mod.info")
	return fileExists(fName)
}

func (r Runner) InitializeWorkshopDir() error {
	if err := r.createPreviewIfNotExist(); err != nil {
		return err
	}

	if err := r.createWorkshopTXT(); err != nil {
		return err
	}

	var dirMod os.FileMode = 0744
	if err := os.MkdirAll(r.ModDir(), dirMod); err != nil {
		return err
	}

	return r.InitializeModDir()
}

func (r Runner) createWorkshopTXT() error {
	var contents = `version=1
id=
title=True Music Addon: *write unique name*
description=*write unique description*
tags=Build 41;Multiplayer;Pop Culture
visibility=unlisted`
	workshopTXT := filepath.Join(r.WorkshopRootDir(), "workshop.txt")
	fileExists, err := exists(workshopTXT)
	if err != nil {
		r.config.Log.Err(err).Str("file", workshopTXT).Msg("failed to stat file")
		return err
	}
	if fileExists {
		return nil
	}

	f, err := os.OpenFile(workshopTXT, os.O_TRUNC|os.O_CREATE|os.O_RDWR, 0644)
	if err != nil {
		r.config.Log.Err(err).Str("file", workshopTXT).Msg("failed to open file")
		return err
	}
	defer func() {
		if err := f.Close(); err != nil {
			r.config.Log.Err(err).Str("file", workshopTXT).Msg("failed to close file")
		}
	}()
	_, err = f.WriteString(contents)
	if err != nil {
		r.config.Log.Err(err).Str("file", workshopTXT).Msg("failed to write to file")
		return err
	}

	return nil
}

// InitializeModDir will create a skeleton mod in the rundir
func (r Runner) InitializeModDir() error {
	// create mod.info
	if err := r.createModInfoIfNotExist(); err != nil {
		return err
	}
	// create poster.png
	if err := r.createPosterIfNotExist(); err != nil {
		return err
	}
	// create media dir
	if err := r.createMediaDir(); err != nil {
		return err
	}
	return nil
}

func (r Runner) createModInfoIfNotExist() error {
	// ask some questions ?? or not
	modInfoFName := filepath.Join(r.ModDir(), "mod.info")
	modInfoExists, err := exists(modInfoFName)
	if err != nil {
		return err
	}
	if modInfoExists {
		return nil
	}
	// use a template to produce mod.info content
	modName := r.WorkshopName()
	modID := r.WorkshopName()
	modPoster := "poster.png"
	modRequire := "truemusic"
	modDescription := `enter some meaningful description here
Can be multiline
Without limits...`
	modInfo := NewModInfo(r.config.Log, modName, modID, modPoster, modRequire, modDescription)
	// create file
	return modInfo.Serialize(modInfoFName)
}

func (r Runner) createImageIfNotExist(name string, width, height int) error {
	fNameExists, err := exists(name)
	if err != nil {
		return err
	}
	if fNameExists {
		return nil
	}

	upLeft := image.Point{X: 0, Y: 0}
	lowRight := image.Point{X: width, Y: height}

	img := image.NewRGBA(image.Rectangle{Min: upLeft, Max: lowRight})

	// Colors are defined by Red, Green, Blue, Alpha uint8 values.
	cyan := color.RGBA{R: 100, G: 200, B: 200, A: 0xff}

	// Set color for each pixel.
	for x := 0; x < width; x++ {
		for y := 0; y < height; y++ {
			switch {
			case x < width/2 && y < height/2: // upper left quadrant
				img.Set(x, y, cyan)
			case x >= width/2 && y >= height/2: // lower right quadrant
				img.Set(x, y, color.White)
			default:
				// Use zero value.
			}
		}
	}

	// Encode as PNG.
	f, err := os.Create(name)
	if err != nil {
		r.config.Log.
			Err(err).
			Str("file", name).
			Msg("failed to create new image file")
		return err
	}
	return png.Encode(f, img)
}

func (r Runner) createPreviewIfNotExist() error {
	previewFileName := filepath.Join(r.WorkshopRootDir(), "preview.png")
	width := 256
	height := 256

	return r.createImageIfNotExist(previewFileName, width, height)
}

func (r Runner) createPosterIfNotExist() error {
	// create a placeholder image
	// save it in poster.png
	posterFileName := filepath.Join(r.ModDir(), "poster.png")
	width := 1080
	height := 1080

	return r.createImageIfNotExist(posterFileName, width, height)
}

func (r Runner) createMediaDir() error {
	mediaDir := r.ActualMediaDir()
	mediaExists, err := exists(mediaDir)
	if err != nil {
		r.config.Log.Err(err).Msg("failed to test if media dir exists")
		return err
	}
	if mediaExists {
		r.config.Log.Error().
			Str("dir", mediaDir).
			Msg("cannot create already existing dir")
		return fmt.Errorf("cannot create already existing dir: %s", mediaDir)
	}

	// create media dir if not exists
	var dirMod os.FileMode = 0744
	if err := os.Mkdir(mediaDir, dirMod); err != nil {
		r.config.Log.Err(err).Msg("failed to create media dir")
		return err
	}
	r.config.Log.Info().Str("dir", mediaDir).Msg("media dir created")
	// then call create subdirs funcs
	if err := r.cleanMediaAutoDirs(); err != nil {
		return err
	}
	// then create the music dirs
	if err := r.createMusicDirIfNotExist(dirMod); err != nil {
		return err
	}
	return nil
}

type PlayerData struct {
	Medias []*GameMedia
	Covers []*Cover
}

// resizeCover take a *Cover and output a resized texture to the proper directory
func (r Runner) resizeCover(cover *Cover) error {
	r.config.Log.Info().Str("cover", cover.Name).Msg("cover added")
	mediaDir := filepath.Dir(r.ActualMusicDir())
	outCover := filepath.Join(mediaDir, "textures", "WorldItems", cover.Name+".png")
	r.config.Log.Info().Str("cover", cover.Name+".png").Msg("created media")
	outCoverFile, err := os.Create(outCover)
	if err != nil {
		r.config.Log.Err(err).Str("media", outCover).Msg("failed to create cover media")
		return err
	}
	defer func() {
		if err := outCoverFile.Close(); err != nil {
			r.config.Log.Err(err).Str("media", outCover).Msg("failed to close cover file")
		}
	}()
	return cover.ToMediaFile(outCoverFile)
}

func (r Runner) createSong(currentDir, fileName string, mediaType MediaType) (*Song, error) {
	f, err := os.Open(filepath.Join(currentDir, fileName))
	if err != nil {
		r.config.Log.Err(err).Str("file", fileName).Msg("failed to open ogg file for metadata check")
		return nil, err
	}
	defer func() {
		if err := f.Close(); err != nil {
			r.config.Log.Err(err).Str("file", fileName).Msg("failed to close file")
		}
	}()
	metaData, err := NewMetadata(f)
	if err != nil {
		r.config.Log.Err(err).Str("file", fileName).Msg("failed to parse metadata")
		return nil, err
	}
	if metaData.HasPicture() {
		// This is just a warning, and the mod will still be generated...
		// but this particular song will most certainly fail to play in game
		r.config.Log.Warn().Str("file", fileName).Msg("ogg file should not contain embedded image")
	}
	return NewSong(r.config.MusicDir, mediaType, fileName, nil), nil
}

func (r Runner) createSongsAndCovers(currentDir string, mediaType MediaType, entries []os.DirEntry) (Songs, Covers, error) {
	var songs Songs
	var covers, tentativeCovers Covers

	for _, entry := range entries {
		if entry.IsDir() {
			r.config.Log.Error().Str("dir", currentDir).Msg("directory cannot contain another directory")
			return nil, nil, fmt.Errorf("dir %s cannot contain another directory", currentDir)
		}
		fileName := entry.Name()
		switch {
		case strings.HasSuffix(fileName, ".ogg"):
			song, err := r.createSong(currentDir, fileName, mediaType)
			if err != nil {
				return nil, nil, err
			}
			songs = append(songs, song)

		case mediaType == MediaTypeVinyl && (strings.HasSuffix(fileName, "jpg") || strings.HasSuffix(fileName, "jpeg") || strings.HasSuffix(fileName, "gif") || strings.HasSuffix(fileName, "png")):
			// cover support is for vinyl only
			tentativeCover, err := NewCover(r.config.Log, filepath.Join(r.ActualVinylPlayerDir(), fileName))
			if err != nil {
				return nil, nil, err
			}
			tentativeCovers = append(tentativeCovers, tentativeCover)
		default:
			r.config.Log.Warn().Str("dir", currentDir).Msg("music directory should only contain ogg files or covers")
		}
	}

	// keep only covers that match a song
	for _, song := range songs {
		// scan all covers and make sure we have a corresponding song
		for _, cover := range tentativeCovers {
			if cover.Name == song.Name {
				// we have a matching cover
				covers = append(covers, cover)
				// make song aware of cover to allow for correct WorldStaticModel
				song.AddCover(cover)
				break
			}
		}
	}
	return songs, covers, nil
}

func (r Runner) ProcessDir(mediaType MediaType) error {
	var songs Songs
	var covers Covers
	var currentDir string
	switch mediaType {
	case MediaTypeTape:
		currentDir = r.ActualBoomBoxDir()
	case MediaTypeVinyl:
		currentDir = r.ActualVinylPlayerDir()
	default:
		panic("unsupported media type")
	}

	// list all files in BoomBox/Vinylplayer dir as Songs
	entries, err := os.ReadDir(currentDir)
	if err != nil {
		r.config.Log.Err(err).Str("dir", currentDir).Msg("failed to read dir contents")
		return err
	}
	// create a list of songs
	songs, covers, err = r.createSongsAndCovers(currentDir, mediaType, entries)
	if err != nil {
		return err
	}

	// process all covers
	for _, cover := range covers {
		if err := r.resizeCover(cover); err != nil {
			return err
		}
	}

	// for each song generate the boombox files
	var medias []*GameMedia
	var sounds []*Sound
	var stores []Store
	for _, song := range songs {
		r.config.Log.Info().Str(getMediaTypeName(mediaType), song.Name).Msg("song added")
		medias = append(medias, song.Media())
		sounds = append(sounds, song.Sound())
		stores = append(stores, song.Stores()...)
	}

	// use templates to gen files
	tmplPlayer, err := getPlayerScriptTemplate(r.config.Log)
	if err != nil {
		return err
	}
	tmplSound, err := getSoundTemplate(r.config.Log)
	if err != nil {
		return err
	}
	tmplLuaShared, err := getSharedLuaTmpl(r.config.Log, mediaType)
	if err != nil {
		return err
	}
	tmplLuaServerItems, err := getServerItemsLuaTemplate(r.config.Log)
	if err != nil {
		return err
	}
	tmplVehicleDistribution, err := getVehicleDistributionTemplate(r.config.Log)
	if err != nil {
		return err
	}

	// prepare output files
	var soundScriptFileName string
	var playerScriptFileName string
	var luaSharedFileName string
	var luaDistributionFileName string
	var luaVehicleDistributionFileName string

	// TODO: accept a steam ID as UID instead of creating a random one each time
	uid, err := uuid.V4()
	if err != nil {
		r.config.Log.Err(err).Msg("failed to get uuid.V4")
		return err
	}

	if mediaType == MediaTypeTape {
		soundScriptFileName = filepath.Join(r.ActualMediaDir(), "scripts", fmt.Sprintf("TCGSoundsTCBoombox%s.txt", uid.String()))
		playerScriptFileName = filepath.Join(r.ActualMediaDir(), "scripts", fmt.Sprintf("TCGMusicScriptTCBoombox%s.txt", uid.String()))
		luaSharedFileName = filepath.Join(r.ActualMediaDir(), "lua", "shared", fmt.Sprintf("TCGMusicDefenitionsTCBoombox%s.lua", uid.String()))
		luaDistributionFileName = filepath.Join(r.ActualMediaDir(), "lua", "server", "Items", fmt.Sprintf("TCGLoadingTCBoombox%s.lua", uid.String()))
		luaVehicleDistributionFileName = filepath.Join(r.ActualMediaDir(), "lua", "server", "Items", fmt.Sprintf("TCGVehicleDistributions%s.lua", uid.String()))
	} else if mediaType == MediaTypeVinyl {
		soundScriptFileName = filepath.Join(r.ActualMediaDir(), "scripts", fmt.Sprintf("TCGSoundsTCVinylplayer%s.txt", uid.String()))
		playerScriptFileName = filepath.Join(r.ActualMediaDir(), "scripts", fmt.Sprintf("TCGMusicScriptTCVinylplayer%s.txt", uid.String()))
		luaSharedFileName = filepath.Join(r.ActualMediaDir(), "lua", "shared", fmt.Sprintf("TCGMusicDefenitionsTCVinylplayer%s.lua", uid.String()))
		luaDistributionFileName = filepath.Join(r.ActualMediaDir(), "lua", "server", "Items", fmt.Sprintf("TCGLoadingTCVinylplayer%s.lua", uid.String()))
	}

	soundFile, err := os.OpenFile(soundScriptFileName, os.O_TRUNC|os.O_CREATE|os.O_RDWR, 0644)
	if err != nil {
		return err
	}
	defer func() {
		if err := soundFile.Close(); err != nil {
			r.config.Log.Err(err).Msg("failed to close sound file script")
		}
	}()

	playerFile, err := os.OpenFile(playerScriptFileName, os.O_TRUNC|os.O_CREATE|os.O_RDWR, 0644)
	if err != nil {
		return err
	}
	defer func() {
		if err := playerFile.Close(); err != nil {
			r.config.Log.Err(err).Msg("failed to close player file script")
		}
	}()
	luaSharedFile, err := os.OpenFile(luaSharedFileName, os.O_TRUNC|os.O_CREATE|os.O_RDWR, 0644)
	if err != nil {
		return err
	}
	defer func() {
		if err := luaSharedFile.Close(); err != nil {
			r.config.Log.Err(err).Msg("failed to close lua shared file")
		}
	}()
	luaDistributionFile, err := os.OpenFile(luaDistributionFileName, os.O_TRUNC|os.O_CREATE|os.O_RDWR, 0644)
	if err != nil {
		return err
	}
	defer func() {
		if err := luaDistributionFile.Close(); err != nil {
			r.config.Log.Err(err).Msg("failed to close lua server items file")
		}
	}()

	// only distribute cassettes in vehicles but no vinyls
	if mediaType == MediaTypeTape {
		luaVehicleDistributionFile, err := os.OpenFile(luaVehicleDistributionFileName, os.O_TRUNC|os.O_CREATE|os.O_RDWR, 0644)
		if err != nil {
			return err
		}
		defer func() {
			if err := luaVehicleDistributionFile.Close(); err != nil {
				r.config.Log.Err(err).Msg("failed to close lua vehicle distribution file")
			}
		}()
		if err := tmplVehicleDistribution.Execute(luaVehicleDistributionFile, songs); err != nil {
			r.config.Log.Err(err).Msg("failed to execute lua vehicle distribution template")
			return err
		}
	}

	// execute templates
	data := PlayerData{
		Medias: medias,
		Covers: covers,
	}
	if err := tmplPlayer.Execute(playerFile, data); err != nil {
		r.config.Log.Err(err).Msg("failed to execute playerfile template")
		return err
	}
	if err := tmplSound.Execute(soundFile, sounds); err != nil {
		r.config.Log.Err(err).Msg("failed to execute soundfile template")
		return err
	}
	if err := tmplLuaShared.Execute(luaSharedFile, songs); err != nil {
		r.config.Log.Err(err).Msg("failed to execute lua shared template")
		return err
	}
	if err := tmplLuaServerItems.Execute(luaDistributionFile, stores); err != nil {
		r.config.Log.Err(err).Msg("failed to execute lua server items template")
		return err
	}

	return nil
}
