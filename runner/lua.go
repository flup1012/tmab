package runner

import (
	"crypto/rand"
	"math/big"
	"text/template"

	"github.com/rs/zerolog"
)

func getSharedLuaTmpl(log zerolog.Logger, mediaType MediaType) (*template.Template, error) {
	switch mediaType {
	case MediaTypeTape:
		return getSharedLuaCassettes(log)
	case MediaTypeVinyl:
		return getSharedLuaVinyl(log)
	default:
		panic("unsupported media type")
	}
}

var sharedLuaCassettes = `require "TCMusicDefenitions"
{{range .}}GlobalMusic["{{.ItemName}}"] = "tsarcraft_music_01_62"{{end}}
`

func getSharedLuaCassettes(log zerolog.Logger) (*template.Template, error) {
	return getTemplate(log, "luasharedcassette", sharedLuaCassettes)
}

var sharedLuaVinyls = `require "TCMusicDefenitions"
{{range .}}GlobalMusic["{{.ItemName}}"] = "tsarcraft_music_01_63"{{end}}
`

func getSharedLuaVinyl(log zerolog.Logger) (*template.Template, error) {
	return getTemplate(log, "luasharedvinyl", sharedLuaVinyls)
}

func getTemplate(log zerolog.Logger, name, tmplVar string) (*template.Template, error) {
	tmpl, err := template.New(name).Parse(tmplVar)
	if err != nil {
		log.Err(err).Str("name", name).Msg("failed parse template")
		return nil, err
	}
	return tmpl, nil
}

var serverLuaTemplate = `{{range . -}}
table.insert(ProceduralDistributions.list["{{.StoreName}}"].items, "Tsarcraft.{{.ItemName}}");
table.insert(ProceduralDistributions.list["{{.StoreName}}"].items, {{.Chance}});
{{end}}
`

func getServerItemsLuaTemplate(log zerolog.Logger) (*template.Template, error) {
	return getTemplate(log, "luaserver", serverLuaTemplate)
}

var vehiculeDistributionTemplate = `{{range . -}}
table.insert(VehicleDistributions.GloveBox.items, "Tsarcraft.{{.ItemName}}");
table.insert(VehicleDistributions.GloveBox.items, 0.001);
{{end}}
`

func getVehicleDistributionTemplate(log zerolog.Logger) (*template.Template, error) {
	return getTemplate(log, "vehicleDistribution", vehiculeDistributionTemplate)
}

var commonStoreNames = []string{
	"CrateCompactDiscs",
	"ElectronicStoreMusic",
	"MusicStoreCDs",
	"MusicStoreSpeaker",
	"MusicStoreSpeaker",
}

var cassetteStoreNames = []string{
	"BandPracticeInstruments",
	"BedroomDresser",
	"BedroomSideTable",
	"ClosetShelfGeneric",
	"CrateCompactDiscs",
	"CrateRandomJunk",
	"DeskGeneric",
	"DresserGeneric",
	"ElectronicStoreMusic",
	"FactoryLockers",
	"FireDeptLockers",
	"FitnessTrainer",
	"Gifts",
	"GolfLockers",
	"GymLockers",
	"Hobbies",
	"HolidayStuff",
	"HospitalLockers",
	"LivingRoomShelf",
	"LivingRoomShelfNoTapes",
	"LivingRoomSideTable",
	"LivingRoomSideTableNoRemote",
	"Locker",
	"LockerClassy",
	"MusicStoreCDs",
	"MusicStoreSpeaker",
	"OfficeDesk",
	"OfficeDeskHome",
	"OfficeDrawers",
	"PoliceDesk",
	"PoliceLockers",
	"SchoolLockers",
	"SecurityLockers",
	"ShelfGeneric",
	"WardrobeChild",
	"WardrobeMan",
	"WardrobeManClassy",
	"WardrobeRedneck",
	"WardrobeWoman",
	"WardrobeWomanClassy",
}

var vinylStoreNames = []string{
	"Antiques",
	"BandPracticeInstruments",
	"ClosetShelfGeneric",
	"CrateRandomJunk",
	"LivingRoomShelf",
	"LivingRoomShelfNoTapes",
	"Locker",
	"LockerClassy",
	"PoliceLockers",
	"SecurityLockers",
	"ShelfGeneric",
}

type Store struct {
	StoreName string
	ItemName  string
	Chance    string
}

func NewStore(name, itemName, chance string) Store {
	return Store{
		StoreName: name,
		ItemName:  itemName,
		Chance:    chance,
	}
}

func getStoresForSong(s *Song) []Store {
	var stores []Store
	for _, sn := range commonStoreNames {
		stores = append(stores, NewStore(sn, s.ItemName, "0.1"))
	}
	switch s.MediaType {
	case MediaTypeTape:
		// get 3 random store places
		max := len(cassetteStoreNames)
		for i := 0; i < 3; i++ {
			number, err := rand.Int(rand.Reader, big.NewInt(int64(max)))
			if err != nil {
				panic("failed to read from random source")
			}
			storeName := cassetteStoreNames[number.Int64()]
			stores = append(stores, NewStore(storeName, s.ItemName, "0.2"))
		}
	case MediaTypeVinyl:
		// get 3 random store places
		max := len(vinylStoreNames)
		for i := 0; i < 3; i++ {
			number, err := rand.Int(rand.Reader, big.NewInt(int64(max)))
			if err != nil {
				panic("failed to read from random source")
			}
			storeName := vinylStoreNames[number.Int64()]
			stores = append(stores, NewStore(storeName, s.ItemName, "0.2"))
		}
	default:
		panic("unsupported media type")
	}
	return stores
}
