package runner

import (
	"path/filepath"
)

// Songs are just lists of Song
type Songs []*Song

// Song represents a media file in a specific directory (TCBoombox or TCVinylplayer)
type Song struct {
	MusicDir  string
	MediaType MediaType
	FileName  string
	Name      string
	ItemName  string
	Cover     *Cover
}

// NewSong is a constructor for a Song
func NewSong(musicDir string, mediaType MediaType, fileName string, cover *Cover) *Song {
	dir := filepath.Base(musicDir)
	s := Song{
		MusicDir:  dir,
		MediaType: mediaType,
		FileName:  fileName,
		Name:      filepath.Base(withoutExtension(fileName)),
		Cover:     cover,
	}
	s.ItemName = s.uniqueName()
	return &s
}

// AddCover will add the given cover to this song
func (s *Song) AddCover(cover *Cover) {
	s.Cover = cover
}

func (s *Song) uniqueName() string {
	return genUniqueName(s.Name, s.MediaType, "")
}

// Media produces a *GameMedia from the current Song
func (s *Song) Media() *GameMedia {
	return NewGameMedia(s.ItemName, s.Name, s.MediaType, s.Cover)
}

func (s *Song) Stores() []Store {
	return getStoresForSong(s)
}

// Sound produces a *Sound from the current Song
func (s *Song) Sound() *Sound {
	var ns *Sound
	if s.MediaType == MediaTypeTape {
		ns = NewSound(s.ItemName, s.FileName, filepath.Base(s.MusicDir)+"/"+BoomboxDir)
	} else if s.MediaType == MediaTypeVinyl {
		ns = NewSound(s.ItemName, s.FileName, filepath.Base(s.MusicDir)+"/"+VinylplayerDir)
	}
	return ns
}
