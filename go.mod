module gitlab.com/flup1012/tmab

go 1.20

require (
	github.com/dhowden/tag v0.0.0-20220618230019-adf36e896086
	github.com/getsentry/sentry-go v0.20.0
	github.com/jessevdk/go-flags v1.5.0
	github.com/m4rw3r/uuid v1.0.1
	github.com/rs/zerolog v1.29.0
	golang.org/x/image v0.7.0
	golang.org/x/term v0.7.0
)

require (
	github.com/mattn/go-colorable v0.1.13 // indirect
	github.com/mattn/go-isatty v0.0.17 // indirect
	golang.org/x/sys v0.7.0 // indirect
	golang.org/x/text v0.9.0 // indirect
)
