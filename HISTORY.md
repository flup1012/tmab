# HISTORY

## not yet released

- Now create the full steam workshop layout instead of just
  the zomboid mod folder

## 0.2 (2023-04-28)

- Added support for creating rundir (mod dir) if not exist
- Added support for initializing the run dir if it does not
  contain a mod.info file (we could have a better heuristic but
  this will do for the moment)
- added an init command
- added QOL for windows users to be able to drag & drop a directory
  on the exe and see the result

## 0.1 (2023-04-26)

- initial release
